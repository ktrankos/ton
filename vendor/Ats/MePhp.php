<?php
namespace Ats;

/**
 * Description of MePhp
 *
 * @author watson
 * @date   17-sep-2014
 */

use PHPRouter\RouteCollection;
use PHPRouter\Router;
use PHPRouter\Route;
use RedBeanPHP\Facade;


class MePhp
{

    public static $twig;
    
    public function __construct($twig)
    {
        Facade::setup('mysql:host=localhost;dbname=db_ton','root','');
        MePhp::$twig=$twig;
        $collection = new RouteCollection();
        
        $collection->attach(new Route('/', array(
            '_controller' => 'Ton\Controller\SiteController::Login',
            'methods' => 'GET'
        )));        
        $collection->attach(new Route('/', array(
            '_controller' => 'Ton\Controller\SiteController::MakeLogin',
            'methods' => 'POST'
        )));
        $collection->attach(new Route('listado', array(
            '_controller' => 'Ton\Controller\SiteController::HomePanel',
            'methods' => 'GET'
        )));
        $collection->attach(new Route('fetch', array(
            '_controller' => 'Ton\Controller\ProductsController::FetchAll',
            'methods' => 'GET'
        )));
        $collection->attach(new Route('save', array(
            '_controller' => 'Ton\Controller\ProductsController::SaveProduct',
            'methods' => 'POST'
        )));
        $router = new Router($collection);
        $router->setBasePath('/');
        
        $route = $router->matchCurrentRequest();
    }
}
?>
