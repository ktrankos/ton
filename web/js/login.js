$(function(){
  var LoginModel = Backbone.Model.extend({});
  var LoginView = Backbone.View.extend({

    el: 'form#login',
    events: {
      'submit':'onSubmit',
      'keyup input#user':  'validUser',
      'keyup input#pwd' : 'validPwd'
    },

    onSubmit: function(e)
    {
      
      e.stopPropagation();
      if(this.isValid()) 
      {
        this.$el.off();
        $(this.el).submit();

      }
      return false;
    },
    validUser: function(e)
    {
      var input = $(e.currentTarget);
      this.model.set({user: input.val()});
      input.popover('destroy');
      input.css({border: '1px solid #cccccc'});
      if(input.val().length<=3) {
        input.css({border: '1px solid red'});
        input.popover('show');
        return false;
      }
      return true;
    },
    validPwd: function(e)
    {
      var input = $(e.currentTarget);
      this.model.set({user: input.val()});
      
      input.css({border: '1px solid #cccccc'});
      input.popover('destroy');
      
      if(!/^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])([a-zA-Z0-9]{5,})$/.test(input.val())) {
        input.popover('show');
        input.css({border: '1px solid red'});        
        return false;
      }
      return true;
    },
    initialize: function() {
      
      this.model.set({user: $(this.el).find('input#user').val()});
      //this.listenTo(this.model, "change", this.render);
    },
    isValid: function()
    {
      var validUser = this.validUser({currentTarget: 'input#user'});
      var validPwd = this.validPwd({currentTarget: 'input#pwd'});
      return (validUser && validPwd);
    }

  });
  new LoginView({model: new LoginModel()});
});
