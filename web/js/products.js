$(function(){


  var Router = Backbone.Router.extend({

    routes: {
      'append': 'append',
      '': 'home',
      'home':'home'
    },
    viewhome:null,    
    viewappend:null,
    initialize: function(){
      var products = new Productos();
      this.viewhome = new HomeView({collection: products });
      this.viewappend = new AppendView({home: this.viewhome});
    },
    append: function () {
      $('#listado').hide();
      $('#append').show();
      
      if($('ul.navbar-nav li:eq(0)').hasClass('active')) $('ul.navbar-nav li:eq(0)').removeClass('active');
      if(!$('ul.navbar-nav li:eq(1)').hasClass('active')) $('ul.navbar-nav li:eq(1)').addClass('active');
      this.view = this.viewappend;
    },

    home: function () {

      $('#listado').show();
      $('#append').hide();
      if($('ul.navbar-nav li:eq(1)').hasClass('active')) $('ul.navbar-nav li:eq(1)').removeClass('active');
      if(!$('ul.navbar-nav li:eq(0)').hasClass('active')) $('ul.navbar-nav li:eq(0)').addClass('active');
      this.view = this.viewhome;
    }

  });
///////////////////////producto
  var Product = Backbone.Model.extend({
     id:0,
     coste:0,
     coste_en_euro:0,
     descripcion:'',
     created_at:'',
     urlRoot: 'save',
     initialize: function()
     {
      if(typeof(this.get('coste'))!='undefined')this.set('coste_en_euro', parseInt(this.get('coste')).euro());
     }

  });  
////////////////////////colecion
  var Productos = Backbone.Collection.extend({
    model: Product,
    url: '/fetch'
  });
//////navigation
  var AppendView = Backbone.View.extend({  
    el:'form#productappend',
    options: {
      home: null
    },
    events:{
      'submit':'onSubmit',
      'keyup textarea#descripcion': 'validDescripcion',
      'keyup input#coste': 'validCoste'
      
    },
    initialize: function(options){
      
      this.options.home = options.home;
      $('input#coste').maskMoney();

    },
    onSubmit: function(e){
      e.preventDefault();
      var validDescripcion = this.validDescripcion({currentTarget:'textarea#descripcion'});
      var validCoste =  this.validCoste({currentTarget:'input#coste'});

      if( validDescripcion && validCoste)
      {
        this.$el.find('p.bg-danger:eq(0)').addClass('hidden');
        this.model = new Product();         
        this.model.set({descripcion: $('textarea#descripcion').val()});

        this.model.set({coste: $('input#coste').val()});  
        var self = this;
        this.model.once("sync", function(model, response, options)
        {
            //
            if(response.status){
              self.resetForm();
              self.model.set('created_at', response.created_at.date);
              self.model.set('id', response.id);
              self.model.set('coste_en_euro', response.coste_en_euro);
              self.$el.find('p.bg-success:eq(0)').fadeOut(0).removeClass('hidden').fadeIn(110).delay(2100).fadeOut(110);

              self.addToList();
            }else{
              self.$el.find('p.bg-danger:eq(0)').removeClass('hidden');
            }
            
            
        });
        this.model.save();
        //this.model.on('save', this.addToList, this);
        this.listenTo(this.model, 'save', this.addToList);
        this.$el.find('button:eq(0)').button('loading');
       
      }
      
      
    },
    addToList: function(e)
    {
      
      this.$el.find('button:eq(0)').button('reset');
      this.options.home.addProduct(this.model);  
    },
    validDescripcion: function(e)
    {

      var input = $(e.currentTarget);
      
      input.popover('destroy');
      input.css({border: '1px solid #cccccc'});
      if(!/^\s*\S+.*/.test(input.val())) {
        input.css({border: '1px solid red'});
        input.popover('show');
        return false;
      }
      return true;
    },
    resetForm: function(){
      this.$el.find('input, textarea').val('');
    },
    validCoste: function(e)
    {
      var input = $(e.currentTarget);     
      input.popover('destroy');
      input.css({border: '1px solid #cccccc'});

      if(input.val()=='0.00' || input.val().length==0) {
        input.css({border: '1px solid red'});
        input.popover('show');
        return false;
      }
      return true;
    }

  });
//////home view
  var HomeView = Backbone.View.extend({
    el:'#listado',
    tagName: 'ul',
    initialize: function() 
    {
      this.collection.bind('reset', this.render, this);
      this.collection.fetch({reset:true});
      
    },
    addProduct: function(product)
    {
      this.collection.push(product);
      
      var _tr = $('<tr/>');
      _tr.append('<td>'+product.get('id')+'</td>');
      _tr.append('<td>'+product.get('descripcion')+'</td>');
      _tr.append('<td>'+parseInt(product.get('coste_en_euro')).euro()+' &euro;</td>');
      _tr.append('<td>'+product.get('created_at')+'</td>');

      $('.table-bordered').append(_tr);
      /*var _tpl = _.template($('#product-list-tpl').html())({products: this.collection.models});
      console.log(_tpl);
      //this.$el.find('.list-group:eq(0)').html(_tpl);*/


    },
    render: function() 
    {
        this.$el.find('.list-group:eq(0)').html(_.template($('#product-list-tpl').html())({products: this.collection.models}));
    }
  });

  var router = new Router();
  Backbone.history.start();
});
Number.prototype.euro = function() {
    var _e = this.valueOf()/100;
    _e = _e.toString()    
    var _m = /(\d+)\.(\d+)/.exec(_e);
    //console.log(_m);
    var part0 = _m[1].replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    return part0+'.'+_m[2];
};