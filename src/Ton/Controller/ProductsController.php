<?php
namespace Ton\Controller;

/**
 * Description of SiteController
 *
 * @author watson
 * @date   17-sep-2014
 */
use Ats\MePhp;
use Spot\Config;
use RedBeanPHP\Facade;


class ProductsController {


    public function FetchAll()
    {
        
        $products = Facade::findAll('products',
        ' ORDER BY id ASC ');
        $return = array();
        foreach ($products as $product) {
            $return[]=array(
                'id'=>$product->id,
                'coste'=>$product->coste,
                'descripcion'=>$product->descripcion,
                'created_at'=>$product->created_at
            ); 
        }
        
        echo json_encode($return);     
    }
    public function SaveProduct(){
        $data = json_decode(file_get_contents('php://input'));
        $product = Facade::dispense('products');
        
        $now = new \DateTime();
        $product->coste = str_replace(array(',', '.'), array('', ''), $data->coste);
        $product->descripcion = $data->descripcion;
        $product->created_at = $now->format('Y-m-d h:m:s');
        $id =  Facade::store($product);
        echo ($id!=FALSE) ? json_encode(array('status'=>true, 'id'=>$id, 'created_at'=>$now, 'coste_en_euro'=> $product->coste)): json_encode(array('status'=>false));
    }
    
}
?>
